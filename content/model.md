## Model

### UserInfo

Property | Type
---|---
`id` | string
`userName` | string
`fullName` | string
`phoneNumber` | string
`email` | string
`avatar` | string

### PointInformation

Property | Type | Description
---|---|---
`id` | string | id of point, required
`name` | string | name of point
`address` | string | address
`province` | string |
`district` | string |
`longitude` | double |
`latitude` | double |
`transshipmentId` | string | id transshipment point
`pointType` | string | 
`price` | double | price of transshipment point
`transshipmentDriver` | string | driver transshipment
`pickUpDropOffType` | integer |  [here](/?language=cURL#pickupdropofftype)

### InformationBySeat

Property | Type | Description
---|---|---
`seatId` | string | seat id
`isAdult` | string | adult is true, children is false
`fullName` | string | full name
`insurancePrice` | string | insurance price
`originalTicketPrice` | string | original price
`extraPrice` | double | 
`mealPrices` | list double | meal price 
`pointUp` | [PointInformation](/?language=cURL#pointinformation) | point up
`pointDown` | [PointInformation](/?language=cURL#pointinformation) | point down

### SortSelection
Property | Type | Description
---|---|---
`fieldName` | string | order field
`ascDirection` | boolean | is asc order