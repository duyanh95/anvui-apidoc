## PlanForTrip

**@author: Bảo Quang Tử**

### Create PlanForTrip

        Create a new record

```endpoint
POST /planfortrip/create
```

#### Example request

```json
$ Header:
  - DOBODY6969: token
  - Content-Type: application/json
```

Property | Type | Required | Description
---|---|---|---
`routeId` | string | 1 | the routeId of plan
`vehicleId` | string | 1 | the vehicle of plan
`startDate` | integer | 1 | the startDate of plan
`endDate` | integer | 1 | the endDate of plan (-1 when unlimit)
`startTime` | integer | 1 | the startTime of trip
`indexCycle` | list integer | 1 | the vehicle of plan
`limitFind` | integer | 0 | the limitFind of plan, number after day allow book ticket(-1 when unlimit)
`drivers` | list [UserInfo](/?language=cURL#userinfo) | 0 | list driver of trip
`assistants` | list [UserInfo](/?language=cURL#userinfo) | 0 | list assistants of trip

```json
{
  "routeId": "R04k7ffYgqQ8k",
  "vehicleId": "V01gWTKqZxeLz",
  "startDate": 20181129,
  "endDate": -1,
  "startTime": 36000000,
  "indexCycle": [0, 1, 2],
  "drivers": [{
    "userId": "DRI07f1pv8CRFcnlz",
    "userName": "manh.laixe",
    "fullName": "Phan Bá Mạnh",
    "phoneNumber": "936889944",
    "email": "",
    "avatar": "https://storage.googleapis.com/staging.dobody-anvui.appspot.com/anvui.jpg"
  }],
  "assistants": [{
    "userId": "DRI07f1pv8CRFcnlz",
    "userName": "manh.laixe",
    "fullName": "Phan Bá Mạnh",
    "phoneNumber": "936889944",
    "email": "",
    "avatar": "https://storage.googleapis.com/staging.dobody-anvui.appspot.com/anvui.jpg"
  }]
}
```

#### Example response

```json
{
    "code": 201,
    "count": 1,
    "status": "success",
    "type": "none",
    "results": {
        "planForTrip": {
            "id": "PLT08q1qO7xgQaXeN",
            "companyId": "TC08Z1qHHZBxlNLt",
            "routeId": "R08a1qHo3SImjAe",
            "seatMapId": "SM08a1qHh95GnEPA",
            "vehicleTypeId": "VT08a1qHgD5d891U",
            "drivers": [],
            "assistants": [],
            "isLunarCalendar": false,
            "startDate": 20190125,
            "endDate": 20190131,
            "limitFind": -1,
            "startTime": 36000000,
            "runTime": 10800000,
            "isActive": true,
            "cycle": 7,
            "indexCycle": [
                0,
                1,
                2,
                3
            ],
            "numberOfDateRun": 1,
            "pointActives": [
                {
                    "startPointId": "PT1C_HFPcdqzwZ",
                    "endPointId": "P04Y1dbmHhilJM",
                    "priceByPoint": 50000,
                    "startDate": 20190125,
                    "startTime": 36000000,
                    "runTime": 5400000,
                    "startPoint": {
                        "id": "PT1C_HFPcdqzwZ",
                        "name": "Bến xe Tiền Hải",
                        "address": "355 Phố Hùng Thắng - Tiền Hải - Thái Bình",
                        "province": " Thái Bình",
                        "district": "",
                        "longitude": 0,
                        "latitude": 0,
                        "pointType": 0
                    },
                    "endPoint": {
                        "id": "P04Y1dbmHhilJM",
                        "name": "Trạm thu phí Liêm Tuyền",
                        "address": "ĐCT Cầu Giẽ - Ninh Bình, Liêm Tuyền, Thanh Liêm, Hà Nam, Việt Nam",
                        "province": "Hà Nam",
                        "district": "Chọn quận huyện",
                        "longitude": 106.923045,
                        "latitude": 17.045081,
                        "pointType": 0
                    },
                    "lock": [],
                    "indexOfDay": 0
                }
            ],
            "createdDate": 1548496069969,
            "updatedDate": 1548496069969,
            "vehicleId": "V08a1qHluTJpVn1",
            "timeZone": 7
        }
    }
}
```

### Get List PlanForTrip

        Get list PlanForTrip

```endpoint
POST /planfortrip/getList
```

#### Example request

```json
$ Header:
  - DOBODY6969: token
  - Content-Type: application/json
```

Property | Type | Required | Description
---|---|---|---
`page` | string | 1 | skip record (must be >= 0)
`count` | integer | 1 | limit record (>= 1 && <= 50)
`routeId` | string | 0 | the routeId of plan

```json
{
	"page": 0,
	"count": 1
}
```

#### Example response

```json
{
    "code": 200,
    "count": 1,
    "status": "success",
    "type": "none",
    "results": {
        "planForTrips": [
            {
                "id": "PLT08q1qO7xgQaXeN",
                "companyId": "TC08Z1qHHZBxlNLt",
                "routeId": "R08a1qHo3SImjAe",
                "seatMapId": "SM08a1qHh95GnEPA",
                "vehicleTypeId": "VT08a1qHgD5d891U",
                "isLunarCalendar": false,
                "startDate": 20190125,
                "endDate": 20190131,
                "limitFind": -1,
                "startTime": 36000000,
                "runTime": 10800000,
                "isActive": true,
                "cycle": 7,
                "indexCycle": [
                    0,
                    1,
                    2,
                    3
                ],
                "numberOfDateRun": 1,
                "pointActives": [
                    {
                        "startPointId": "PT1C_HFPcdqzwZ",
                        "endPointId": "P04Y1dbmHhilJM",
                        "priceByPoint": 50000,
                        "startDate": 20190125,
                        "startTime": 36000000,
                        "runTime": 5400000,
                        "startPoint": {
                            "id": "PT1C_HFPcdqzwZ",
                            "name": "Bến xe Tiền Hải",
                            "address": "355 Phố Hùng Thắng - Tiền Hải - Thái Bình",
                            "province": " Thái Bình",
                            "district": "",
                            "longitude": 0,
                            "latitude": 0,
                            "pointType": 0
                        },
                        "endPoint": {
                            "id": "P04Y1dbmHhilJM",
                            "name": "Trạm thu phí Liêm Tuyền",
                            "address": "ĐCT Cầu Giẽ - Ninh Bình, Liêm Tuyền, Thanh Liêm, Hà Nam, Việt Nam",
                            "province": "Hà Nam",
                            "district": "Chọn quận huyện",
                            "longitude": 106.923045,
                            "latitude": 17.045081,
                            "pointType": 0
                        },
                        "lock": [],
                        "indexOfDay": 0
                    }
                ],
                "createdDate": 1548496069969,
                "updatedDate": 1548496069969,
                "timeZone": 7
            }
        ]
    }
}
```
### Search Trip

        Search trip

```endpoint
POST /planfortrip/search
```

#### Example request

```json
$ Header:
  - DOBODY6969: token
  - Content-Type: application/json
```

Property | Type | Required | Description
---|---|---|---
`page` | string | 1 | skip record (must be >= 0)
`count` | integer | 1 | limit record (>= 1)
`routeId` | string | 0 | filter by routeId
`companyId` | string | 0 | filter by companyId
`vehicleTypeId` | string | 0 | filter by vehicleTypeId
`startTime` | integer | 0 | filter by startTime of trip
`range` | string | 0 | filter startTime with range
`searchPointOption` | integer | 1 | [here](/?language=cURL#searchpointoption)
`startPoint` | string | 1 | point up
`endPoint` | string | 1 | point down
`date` | integer | 1 | date of trip
`platform` | integer | 1 | 1 <= platform <= 4
`sortSelections` | list [SortSelection](/?language=cURL#sortselection) | 0 | sort selections
`timeZone` | integer | 0 | timeZone


```json
{
	"date": 20190125,
	"sortSelections": [{
		"fieldName": "startTime",
		"ascDirection": true
	}],
	"startPoint": "PT1C_HFPcdqzwZ",
	"endPoint": "PT1C_HFNLNdYfs",
	"routeId": "R08a1qHo3SImjAe",
	"companyId": "TC08Z1qHHZBxlNLt",
	"searchPointOption": 1,
	"count": 100,
	"timeZone": 7,
	"page": 0,
	"platform": 1
}
```

#### Example response

```json
{
	"code": 200,
	"count": 1,
	"status": "success",
	"type": "none",
	"results": {
		"trips": [{
			"tripId": "TR08q1qO8imwN6cZ",
			"planId": "PLT08q1qO8iim7UpM",
			"tripStatus": 1,
			"vehicleTypeId": "VT08a1qHgD5d891U",
			"vehicleTypeName": "Xe giường nằm",
			"date": 20190125,
			"startTime": 10800000,
			"runTime": 10800000,
			"price": 100000,
			"seatMap": {
				"seatMapId": "SM08a1qHh95GnEPA",
				"vehicleTypeId": "VT08a1qHgD5d891U",
				"seatMapName": "Giường nằm 40 chỗ",
				"numberOfRows": 8,
				"numberOfColumns": 5,
				"numberOfFloors": 2,
				"type": 1,
				"companyId": "TC08Z1qHHZBxlNLt",
				"status": 2,
				"isSample": false,
				"seatList": [{
					"seatId": "TÀI",
					"row": 1,
					"column": 1,
					"floor": 1,
					"seatStatus": 1,
					"seatType": 2,
					"overTime": 0,
					"listTicketId": [],
					"listUserId": [],
					"listTicketCode": [],
					"extraPrice": 0,
					"images": [
						""
					]
				}]
			},
			"pointUp": {
				"id": "PT1C_HFPcdqzwZ",
				"name": "Bến xe Tiền Hải",
				"address": "355 Phố Hùng Thắng - Tiền Hải - Thái Bình",
				"province": " Thái Bình",
				"district": "",
				"longitude": 0,
				"latitude": 0,
				"pointType": 0
			},
			"pointDown": {
				"id": "PT1C_HFNLNdYfs",
				"name": "Bến Xe Mỹ Đình",
				"address": "20 Phạm Hùng, P Mỹ Đình - Nam Từ Liêm - Hà Nội",
				"province": " Hà Nội",
				"district": "",
				"longitude": 105.7782984,
				"latitude": 21.028489,
				"pointType": 0
			}
		}]
	}
}
```