## Ticket

**@author: Bảo Quang Tử**

### Book ticket

        Create a new record
    
```endpoint
POST /ticket/book
```

#### Example request

```json
$ Header:
  - DOBODY6969: token
  - Content-Type: application/json
```

Property | Type | Required | Description
---|---|---|---
`tripId` | string | 1 | Chuyến của vé
`listSeatId` | list string | 1 | Danh sách ghế
`fullName` | string | 1 | Tên khách hàng
`phoneNumber` | string | 1 | Số điện thoại người đặt vé
`email`  | string | 1 | email
`overTime` | long | 1 | Thời gian kết thúc giữ chỗ milisecond (0 là giữ chỗ ưu tiên)
`agencyPrice` | double | 1 | Thực thu
`paidMoney` | double | 0 | Số tiền đã thanh toán
`platform` | integer | 1 | Kênh đặt vé
`pointUp` | [PointInformation](/?language=cURL#pointinformation) | 1 | Điểm lên
`pointDown` | [PointInformation](/?language=cURL#pointinformation) | 1 | Điểm xuống
`informationBySeats` | list [InformationBySeat](/?language=cURL#informationbyseat) | 0 | Thông tin vé theo từng ghế

```json
{
	"tripId": "TR08p1qNgmIO625K",
	"listSeatId": ["B51"],
	"fullName": "Bảo Quang Tử",
	"phoneNumber": "0978991320",
	"overTime": 0,
	"agencyPrice": 100000,
	"paidMoney": 0.0,
	"pointUp": {
		"id": "PT1C_HFPcdqzwZ",
		"name": "Bến xe Tiền Hải",
		"address": "355 Phố Hùng Thắng - Tiền Hải - Thái Bình",
		"province": " Thái Bình",
		"district": "",
		"longitude": 0,
		"latitude": 0,
		"pointType": 0
	},
	"pointDown": {
		"id": "PT1C_HFNLNdYfs",
		"name": "Bến Xe Mỹ Đình",
		"address": "20 Phạm Hùng, P Mỹ Đình - Nam Từ Liêm - Hà Nội",
		"province": " Hà Nội",
		"district": "",
		"longitude": 105.7782984,
		"latitude": 21.028489,
		"pointType": 0
	},
	"platform": 1
}
```

#### Example response

```json
{
    "code": 202,
    "count": 1,
    "status": "success",
    "type": "none",
    "results": {
        "ticket": {
            "ticketId": "TK08r1qOPNDjOGhc",
            "ticketCode": "190127-388159",
            "createdDate": 1548557387941,
            "createdDateInt": 20190127,
            "tripId": "TR08p1qNgmIO625K",
            "scheduleId": "PLT08p1qNgkIqzD5Q",
            "companyId": "TC08Z1qHHZBxlNLt",
            "getInTimePlan": 36000000,
            "getInTimePlanInt": 20190126,
            "listSeatId": [
                "B51"
            ],
            "indexFillRoute": [
                0,
                1
            ],
            "fullName": "Bảo Quang Tử",
            "phoneNumber": "0978991320",
            "ticketStatus": 7,
            "overTime": 0,
            "informationBySeats": [
                {
                    "seatId": "B51",
                    "isAdult": true,
                    "fullName": "Bảo Quang Tử",
                    "originalTicketPrice": 100000,
                    "extraPrice": 0,
                    "pointUp": {
                        "id": "PT1C_HFPcdqzwZ",
                        "name": "Bến xe Tiền Hải",
                        "address": "355 Phố Hùng Thắng - Tiền Hải - Thái Bình",
                        "province": " Thái Bình",
                        "district": "",
                        "longitude": 0,
                        "latitude": 0,
                        "pointType": 0
                    },
                    "pointDown": {
                        "id": "PT1C_HFNLNdYfs",
                        "name": "Bến Xe Mỹ Đình",
                        "address": "20 Phạm Hùng, P Mỹ Đình - Nam Từ Liêm - Hà Nội",
                        "province": " Hà Nội",
                        "district": "",
                        "longitude": 105.7782984,
                        "latitude": 21.028489,
                        "pointType": 0
                    }
                }
            ],
            "originalTicketPrice": 100000,
            "paymentTicketPrice": 100000,
            "paidMoney": 0,
            "pointUp": {
                "id": "PT1C_HFPcdqzwZ",
                "name": "Bến xe Tiền Hải",
                "address": "355 Phố Hùng Thắng - Tiền Hải - Thái Bình",
                "province": " Thái Bình",
                "district": "",
                "longitude": 0,
                "latitude": 0,
                "pointType": 0
            },
            "pointDown": {
                "id": "PT1C_HFNLNdYfs",
                "name": "Bến Xe Mỹ Đình",
                "address": "20 Phạm Hùng, P Mỹ Đình - Nam Từ Liêm - Hà Nội",
                "province": " Hà Nội",
                "district": "",
                "longitude": 105.7782984,
                "latitude": 21.028489,
                "pointType": 0
            },
            "seller": {
                "id": "ORG08Z1qHHZBxkyEf",
                "userName": "xeanvui",
                "fullName": "Bảo Quang Tử",
                "phoneNumber": "978991320",
                "email": "",
                "avatar": "https://storage.googleapis.com/staging.dobody-anvui.appspot.com/anvui.jpg"
            },
            "paymentType": 2,
            "usingOpenPrice": false,
            "userId": "ORG08Z1qHHZBxkyEf",
            "platform": 1,
            "agencyPrice": 100000,
            "sendSMS": true,
            "routeInfo": {
                "id": "R08a1qHo3SImjAe",
                "name": "Thái Bình - Hà Nội",
                "nameShort": "TB - HN",
                "phoneNumber": "0978991320",
                "images": [
                    "0"
                ],
                "displayPrice": 100000
            },
            "routeId": "R08a1qHo3SImjAe"
        }
    }
}
```