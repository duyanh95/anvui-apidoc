## Enum

### SearchPointOption

Value | Description
---|---
`1` | pointId to pointId
`2` | province district
`3` | text to text

### PickUpDropOffType

Value | Description
---|---
`0` | normal point
`1` | pick up, drop off home
`2` | along the way
`3` | transshipment